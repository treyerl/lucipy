from lucipy import LcClient, OnResponse, Message

"""
For testing error handling upon binary errors using a Haskell test server from https://github.com/achirkin/qua-kit
/libs/hs/luci-connect
"""

def do(x):
    print("got", x)
    c.send(Message({'back': 'to you'}))

c = LcClient(OnResponse(do))
c.connect()
