import sys
from lucipy import RemoteService, RemoteServiceResponseHandler, Message, SysArgvProcessor

class FileEchoHandler(RemoteServiceResponseHandler):

    def handleRun(self, message):
        h = message.getHeader()
        del h['run']
        callID = h.pop('callID')
        print(h)
        s.send(Message({'result': h, 'callID': callID}))

savp = SysArgvProcessor(sys.argv, host="localhost", port=7654)

s = RemoteService(
    serviceName="RemoteFileEcho",
    inSpec={'ANY key': 'attachment'},
    outSpec={'ANY key': 'attachment'},
    exampleCall={'run': 'RemoteFileEcho', 'file1': '<attachment>'},
    description="This is a remote file echo service. It's an example to show how to implement a Luci service in python.",
    responseHandler=FileEchoHandler,
    id=savp.id,
    io=savp.io
    )

s.connect(host=savp.hostname, port=savp.port,
          callback=lambda m: print("Registered as "+m.getHeader()['result']['registeredName']))