#Lucipy#

This is a 400 lines python library to connect to [Luci](https://bitbucket.org/treyerl/luci2/) as a client or as a remote service.

Look at the example below (example.py) to get an idea of how to implement your own remote luci service.

```
#!python

import sys
from lucipy import RemoteService, RemoteServiceResponseHandler, Message, SysArgvProcessor

class FileEchoHandler(RemoteServiceResponseHandler):

    def handleRun(self, message):
        h = message.getHeader()
        del h['run']
        print(h)
        s.respond(Message({'result': h}))

savp = SysArgvProcessor(sys.argv, host="localhost", port=7654)

s = RemoteService(
    serviceName="RemoteFileEcho",
    inSpec={'ANY key': 'attachment'},
    outSpec={'ANY key': 'attachment'},
    exampleCall={'run': 'RemoteFileEcho', 'file1': '<attachment>'},
    description="This is a remote file echo service. It's an example to show how to implement a Luci service in python.",
    responseHandler=FileEchoHandler,
    id=savp.id,
    io=savp.io
    )

s.connect(host=savp.hostname, port=savp.port,
          callback=lambda m: print("Registered as "+m.getHeader()['result']['registeredName']))
```

As you can see, you have to implement a RemoteServiceResponseHandler that handles a run call and represents the implementation of your service. With `s = RemoteService(...)` you specify in and outputs of your service as well as the response handler a name for your service and an example call. As you can tell from the name of the service, Luci messages can contain attachments, while their header is a simple json string. For the sake of simplicity, this very python library saves the attachments to files located in the cwd. (In the future you might be able to specify an attachment folder). 

[More information on Luci messages](https://bitbucket.org/treyerl/luci2/wiki/Luci%20networking%20or%20how%20to%20write%20a%20connection%20library%20in%20your%20preferred%20language).